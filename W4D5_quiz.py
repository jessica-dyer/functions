class Car:
  def __init__(self, make, model, year):
      self.make = make
      self.model = model
      self.year = year

  def __str__(self):
    string = str(self.year) + " " + self.make + " " + self.model
    return string

# car = Car("Oldsmobile", "Alero", 2001)
# print(car.__str__())

def horizontal_bar_chart(sentence):
  dictionary = {}

  for letter in sentence:
      if letter not in dictionary and letter != ' ':
        dictionary[letter] = letter
      elif letter != ' ':
        dictionary[letter] += letter
  values = list(dictionary.values())
  values.sort()
  return values

print(horizontal_bar_chart("abba has a banana"))