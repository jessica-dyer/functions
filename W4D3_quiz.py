dict = {
    "hair": "red",
    "eyes": "blue",
}

def make_description(name, attributes):
    if attributes:
        string = name + ", "
        for key in attributes:
            if key == list(attributes.keys())[-1]:
                string += attributes[key] + " " + key
            else:
                string += attributes[key] + " " + key + ", "

        return string
    return name

# print(make_description("Lulu", dict))

def reverse_entries(dictionary):
    newDictionary = {}
    for key, value in dictionary:
        newDictionary[value] = key
    return newDictionary

# reverse_entries(dict)

def reverse_entries(dictionary):
    return {value: key for key, value in dictionary.items()}

print(reverse_entries(dict))


