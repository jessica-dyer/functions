class Person:
    def __init__(self, name):
        self.name = name

list_of_people = [
    Person("Laila"),
    Person("Evander"),
    Person("Talia"),
    Person("Asha"),
]

def names_in_list(a_list, list_of_people_objects):
  result = []
  names_in_objects = []

  for object in list_of_people_objects:
     names_in_objects.append(object.name)

  for name in a_list:
    if name in names_in_objects:
      result.append(True)
    else:
      result.append(False)

  return result


# print(names_in_list(["Asha", "Azami", "Basia"], list_of_people))

