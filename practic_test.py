def partition(list, size):
    chunks = []
    in_progress = []
    for item in list:
        if len(in_progress) == size:
            chunks.append(in_progress)
            in_progress = []
        in_progress.append(item)
    return chunks

input = [0, 1, 2, 3, 4, 5, 6]

# result = partition(input, 2)
# print(result)

def count_matches(items, param2=None):
    matches = []
    for item in items:
        if(item.get("color") == param2):
            matches.append(item)
    return len(matches)

input = [
    {"background": "green",  "size": 5,  "color": "blue"},
    {"background": "yellow", "size": 5,  "color": "green"},
    {"background": "blue",   "size": 25, "color": "green"},
    {"background": "yellow", "size": 5,  "weight": "light"},
]

# result = count_matches(input)
# print(result)

from datetime import datetime
class Invoice:
    def __init__(self, customer_name, amount, invoice_date):
        self.customer_name = customer_name
        self.amount_due = amount
        self.invoice_date = invoice_date

def amount_due(invoices, days=30):
    past_due = 0
    for invoice in invoices:
        if (datetime.now() - invoice.invoice_date).days > days:
            past_due += invoice.amount_due
    return past_due

invoices = [
    Invoice("Raul", 25.00, datetime(2010, 4, 15)),
    Invoice("Poli", 50.00, datetime(2029, 11, 5)),
    Invoice("Don", 75.00, datetime(2012, 7, 21)),
    Invoice("Anne", 100.00, datetime(2035, 6, 17))
]

# result = amount_due(invoices, 90)
# print(result)

def add_all(*numbers):
  total = 0
  for num in numbers:
    total += num
  return total

def divide_bill(bill, num_diners, tip=0.2):
    tipTotal = bill*tip
    totalBill = bill + tipTotal
    amountPerDiner = totalBill/num_diners
    return amountPerDiner

# print(divide_bill(10, 4, 0.1))

def c_to_f(degrees_celsius):
  degreesFarenheit = (degrees_celsius * (9/5)) + 32
  return degreesFarenheit

shipment = [
    {
        "product_name": "beans",
        "product_weight_pounds": 2,
        "quantity": 5,
    },
    {
        "product_name": "rice",
        "product_weight_pounds": 1.5,
        "quantity": 7,
    },
]

def find_shipment_weight(shipment):
    total = 0
    for item in shipment:
      currentProductWeight = item["product_weight_pounds"]
      currentQuantity = item['quantity']
      total += (currentProductWeight * currentQuantity)
    return total

# print(find_shipment_weight(shipment))
strings = ["a", "bbb", "cc"]
def find_longest(strings):
    if len(strings) == 0:
      return None
    longestString = ''
    for string in strings:
      if len(string) > len(longestString):
        longestString = string
    return longestString

def is_multiple_of(number, base):
    if type(base) != int:
        return False

    if number % base == 0:
      return True
    return False

def shift_cipher(message, shift):

    new_string = ""
    for letter in message:
        if letter == "Z":
            new_letter = "A"
            new_string += new_letter
        elif letter == "z":
            new_letter = "a"
            new_string += new_letter
        else:
            unicode = ord(letter)
            new_unicode = unicode + shift
            new_letter = chr(new_unicode)
            new_string += new_letter
    return new_string

# print(shift_cipher("bbc", 3))

input = "1,2,3,4"

def read_delimited(line, separator=","):
  if line == "":
    return [""]
  newList= list(line.split(separator))
  return newList
# print(read_delimited(input))
input = [1,2,3,4,5,6,7]
def pair_up(items):
  finalList = []
  for index in range(1, len(items), 2):
    currentList = []
    previousValue = items[index - 1]
    currentValue = items[index]
    currentList.append(previousValue)
    currentList.append(currentValue)
    finalList.append(currentList)
  return finalList

  # print(pair_up(input))

def join_strings(strings, separator=""):
  newString = ""
  for item in strings:
    newString += item
    if item != strings[-1]:
      newString += separator
  return newString

# print(join_strings(["aaa", "bbb", "ccc"], "--"))

def unique_elements(items):
    listWithDuplicatesRemoved = []
    for item in items:
      if item not in listWithDuplicatesRemoved:
        listWithDuplicatesRemoved.append(item)
    return listWithDuplicatesRemoved

# print(unique_elements([1, 1, 2]))

class A:
    def __init__(self, a):
        self.a = a

    def speak(self):
        print(self.a)

class B:
    def __init__(self, b):
        self.b = b

    def speak(self):
        print(self.b)

class Child(A,B):
    def __init__(self, a, b):
        self.a = a
        self.b = b

child = Child("item1", "item2")

child.speak()