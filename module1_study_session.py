# Question 4: Implement the function add_all() which returns the sum of all of its arguments.
  # Takes: A LIST of numbers
  # Returns: All numbers in the list added together (int or float depending on the numbers in the list)

from functools import reduce


def add_all(numbers):
  total = 0
  for num in numbers:
    total += num
  return total

def add_all(numbers):
  return reduce(lambda x, y: x + y, numbers)

# print(add_all([1, 2, 3]))
# print(add_all([]))

##################################

# Question 5: Implement the function divide_bill which will return the amount due for
# each diner given: a total bill, number of diners, and the desired tip amount.
  # Takes: Bill amount (int), number of diners (int), tip amount (default =0.2) (float)
  # Returns: Amount of money due for each diner (float)

def divide_bill(bill, num_diners, tip=0.2):
  tipTotal = bill*tip
  totalBill = bill + tipTotal
  amountPerDiner = totalBill/num_diners
  return amountPerDiner


bill = 30
numberOfDiners = 5
# Default tip amount
# print(divide_bill(bill, numberOfDiners))

##################################

# Question 6: This function converts temperature measured in degrees Celsius (°C)
# to degrees Fahrenheit (°F). F = (°C * 9/5) + 32
  # Takes: an integer that is degrees in celsius
  # Returns: degrees in F (float)

def c_to_f(degrees_celsius):
  degreesFarenheit = (degrees_celsius * (9/5)) + 32
  return degreesFarenheit

# print(c_to_f(100))

##################################

# Question 7: TOTAL WEIGHT--This function takes a list of items in a shipment and
# calculates the total weight of the shipment. Below is an example shipment list in Python:
  # Takes: List of dictionaries
  # Returns: Total weight (float)

sampleShipment = [
    {
        "product_name": "beans",
        "product_weight_pounds": 2,
        "quantity": 5,
    },
    {
        "product_name": "rice",
        "product_weight_pounds": 1.5,
        "quantity": 7,
    },
]

def find_shipment_weight(shipment):
  total = 0
  for dictionary in shipment:
    currentWeight = dictionary['product_weight_pounds']
    currentQuantity = dictionary['quantity']
    total += currentWeight*currentQuantity
  return total

def find_shipment_weight(shipment):
  totalWeights = [dictionary['product_weight_pounds'] * dictionary['quantity'] for dictionary in shipment]
  return reduce(lambda x, y: x + y, totalWeights)

print(find_shipment_weight(sampleShipment))

##################################

# Question 8: LONGEST STRING--This function takes a list of strings and returns the longest string in the list.
# Note: The input data will have only one longest string.
  # Takes: list of strings
  # Returns: string (that is the longest of the group)

sampleStrings = ["a", "bbb", "cc"]

def find_longest(listOfStrings):
  if len(listOfStrings) == 0:
      return None
  longestString = ''
  for string in listOfStrings:
    if len(string) > len(longestString):
      longestString = string
  return longestString

def find_longest(listOfStrings):
  return reduce(lambda x, y: x if (len(x) > len(y)) else y, listOfStrings)

def find_longest(listOfStrings):
  return max(listOfStrings, key=len)

# print(find_longest(sampleStrings))

##################################

# Question 9: MULTIPLES--This function returns True if number is an integer multiple of base where number
# and base are both integers. If base is an integer multiple of number, then number/base is an integer.
  # Takes: number (int), base (int)
  # Returns: Boolean (True/False)

def is_multiple_of(number, base):
  if type(base) != int:
    return False
  # if number is evenly divisible by base
  return number % base == 0

# print(is_multiple_of(6, 1.5), "should equal False")
# print(is_multiple_of(6, 3),  "should equal True")

##################################

# Question 10: GETTING SNEAKY--This pseudo-encryption method works by shifting each letter in a message a fixed amount.
  # Takes:
  # Returns:

def shift_cipher(message, shift):
  finalString = ''
  for letter in message:
    newLetter = chr(ord(letter) + shift)
    finalString += newLetter
  return finalString

def shift_cipher(message, shift):
  newString = ''.join(map(lambda x: chr(ord(x) + shift), message))
  return newString

# print(shift_cipher("bbc", 3), "should equal eef")

##################################

# Question 11: DELIMITED FILES--This function must take a string that is composed of values that are separated by some
# "delimiter" character, like a comma. It will return an array of the values in the string with the delimiter removed.
# If input is an empty string, the result should be a list with a single empty string in it : [""]
  # Takes: String of things seperatated by a delimiter
  # Returns: List of values in the string with the delimiter removed

def read_delimited(line, separator=","):
  if line == "":
    return [""]
  newList = line.split(separator)
  return newList


sampleInput = "1,2,3,4"
# print(read_delimited(sampleInput))

##################################

# Question 12: MAKE PAIRS--Please complete the pair_up function below so that it takes a list of items and
# returns a list of pairs of consecutive items.
  # Takes: A list of integers
  # Returns: A list of lists where each sub-list is a pair of consecutive items

def pair_up(items):
  finalList = []
  for index in range(1, len(items), 2):
    currentList = []
    previousValue = items[index-1]
    currentValue = items[index]
    currentList.append(previousValue)
    currentList.append(currentValue)
    finalList.append(currentList)
  return finalList

def pair_up(listOfItems):
  if len(listOfItems) % 2 == 1:
    listOfItems.pop()
  return [[listOfItems[i], listOfItems[i+1]] for i in range(0, len(listOfItems), 2)]

# samplePairupInput = [100,200,300,400,500,600,700]
# print(pair_up(samplePairupInput))

##################################

# Question 13: ONE FROM MANY--The join_strings returns a single string that is the concatenation of all
# of the input strings, separated by the separator if supplied.
  # Takes: List of strings, a delimiter string that is a character
  # Returns: A string with the delimiter concatenated between input strings

def join_strings(listOfStrings, separator=""):
    newString = listOfStrings[0]
    for string in listOfStrings[1:]:
      newString += separator + string
    return newString

def join_strings(listOfStrings, separator=""):
  return separator.join(listOfStrings)
# print(join_strings(["aaa", "bbb", "ccc"], "--"))

##################################

# Question 14: NO DUPLICATES--The function below takes a list of items and returns a copy of the list
# with all of the duplicate items removed in the same order that they were encountered in the list.
  # Takes: A list of items
  # Returns: A list of items with duplicates removed

def unique_elements(listOfItems: list):
  listwithDuplicatesRemoved = []
  # look at each number and if we've already seen it, skip it
  for item in listOfItems:
    if item not in listwithDuplicatesRemoved:
      listwithDuplicatesRemoved.append(item)
  return listwithDuplicatesRemoved

def unique_elements(listOfItems: list):
  return sorted(list(set(listOfItems)), key=listOfItems.index)

# print(unique_elements([1,1,2]))

def subtract_all(listOfItems: list):
  return reduce(lambda x, y: x - y, listOfItems)

# print(subtract_all([1, 2, 3]))

def multiplyListWithMap(listOfItems: list):
  return list(map(lambda x: x*10, listOfItems))

def multiplyListWithComp(listOfItems: list):
  return [x * 10 for x in listOfItems]

# print(multiplyListWithMap([1, 2, 3, 4]))
# print(multiplyListWithComp([1, 2, 3, 4]))