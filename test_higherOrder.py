from functools import reduce


def shift_cipher(message, shift):
    if shift == 0:
        return message
    shiftedMessage = ''.join(map(lambda x: chr(ord(x) + shift), message))
    return shiftedMessage

# print(shift_cipher("bbc", -1))

def find_longest(strings):
  string = reduce(lambda x, y: x if (len(x) > len(y)) else y, strings)
  return string

# print(find_longest(["a", "bbb", "cc"]))

def pair_up(items):
    if len(items) % 2 == 1:
      items.pop()
    return [[items[i], items[i+1]] for i in range(0, len(items), 2)]

# print(pair_up([1,2,3,4,5,6,7]))

def add_all(*numbers):
    return reduce(lambda x, y: x + y, numbers)


def myReduce(lambdaFunc, listOfThings):
  if type(listOfThings) == tuple:
    listOfThings = list(listOfThings)
  if len(listOfThings) == 0:
    return None
  accumulator = listOfThings.pop(0)
  for item in listOfThings:
    accumulator = lambdaFunc(accumulator, item)
  return accumulator

# print(add_all([1, 2, 3]))
# print(add_all(1, 2, 4))

def find_longest(strings):
  if len(strings) == 0:
    return None
  longestString = reduce(lambda x, y: x if (len(x) > len(y)) else y, strings)
  return longestString

# print(find_longest(["a", "bbb", "cc"]))

def join_strings(strings, separator=""):
    if len(strings) == 0:
        return ""
    finalString = strings[0]
    for string in strings[1:]:
        finalString += separator + string
    return finalString

def join_strings(strings, separator=""):
  return separator.join(strings)

print(join_strings(["aaa", "bbb", "ccc"], "--"))