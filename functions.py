from turtle import clear


def get_index(char, string):
    counter = 0
    for c in string:
      if char == c:
        return counter
      counter += 1
    return -1

# print(get_index('a', 'I am a hacker'))

def natural_sum(integer):
    sum = 0
    for n in range(integer + 1):
        sum += n
    return sum

output = natural_sum(6)
# print(output) # -> 21

def factorial(integer):
    product = 1
    n = 1
    for n in range(1, integer + 1):
        product *= n
    return product
# print(factorial(3))

def sum_interval(num1, num2):
  if num1 > num2:
    return 0
  sum = 0
  current_num = num1
  while current_num < num2:
    sum += current_num
    current_num += 1
  return sum

def sum_interval(num1, num2):
    # your code here
    sum_ = 0

    for num in range(num1, num2):
        sum_ += num1

    return sum_

# print(sum_interval(2, 5))

# Write a function called add_list_to_dict that has three parameters: dct , key , and lst . A dictionary, a key, and a list, respectively.

# Given the three parameters, the add_list_to_dict function returns a new dictionary with all of the key-value pairs from the original dictionary, and also sets a new key-value pair according to the given parameters. If anything besides a list is given as the third parameter, lst , the function should return a None-typed object.

# my_dct = {'name': 'Smiley'}
# my_list = [1, 3]

def add_list_to_dict(dictionary, key, my_list):
  if type(my_list) != list:
    return None
  new_dict = dictionary.copy()
  new_dict[key] = my_list
  return new_dict


# d = add_list_to_dict(my_dct, 'key', my_list)
# print(d['key']) # --> [1, 3]
# print(d['name']) # --> 'Smiley'

# d2 = add_list_to_dict(my_dct, 'key', "I shouldn't add a string value.")
# print(d2)

def remove_gt(integer, dictionary):
    delete = [key for key in dictionary if type(dictionary[key]) == int and dictionary[key]> integer]
    for key in delete: del dictionary[key]
    return dictionary

# inp = {'a': 8, 'b': 2, 'c': 'montana'}
# print(remove_gt(5, inp))

def check_age(name, age):
    if age <21:
        return "Go home, " + name + "!"
    else:
        return "Welcome, " + name + "!"

# print(check_age("Perla",101))

def any_even (num1, num2):
    if num1 % 2 == 0 or num2 % 2 == 0:
        return True
    else:
        return False

# print(any_even(1,5))

inp = {'name': 'Montana', 'age': 20, 'location': 'Texas'}

def rmv_str_len_lt(num, input):
  delete = [key for key in input if type(input[key]) == str and len(input[key])> num]
  for key in delete: del input[key]
  return input

# rmv_str_len_lt(6, inp)

# print(inp)  # -> {'age': 20, 'location': 'Texas'}

obj = {'key': None}

def get_avg(dictionary, key):
  try:
    if type(dictionary[key]) == list:
      current_list = dictionary[key]
      if len(current_list) == 0:
        return 0
      else:
        return sum(current_list) / len(current_list)
  except KeyError:
    return 0
  if type(dictionary[key]) != list:
    return 0

# output = get_avg(obj, 'key')
# print(output) # --> 2

def valid_cred(name,pass_word):
    if len(name) > 3 and len(pass_word) >=8:
        return True
    else:
        return False
# print(valid_cred("Perla", "starfish"))

def natural_sum(integer):
    if integer == 0:
      return 0
    return natural_sum(integer-1) + integer

# print(natural_sum(6))

def convert_grade(score):
    if score >= 101 or score < 0:
      return "INVALID SCORE"
    elif score < 100 and score >= 90:
      return "A"
    elif score < 90 and score >= 80:
      return "B"
    elif score < 80 and score >= 70:
      return "C"
    elif score < 70 and score >= 60:
      return "D"
    elif score < 60 and score >= 0:
      return "F"
    return "INVALID SCORE"

# print(convert_grade(59))

def print_characters(string):
  for c in string:
    print(c)
print_characters("I am a hacker")